var NotificationHeaderView = function(headers) {
    this.initialize = function() {
        // Define a div wrapper for the view. The div wrapper is used to attach events.
        this.el = document.createElement('div');
    };
    this.render = function() {
        this.el.innerHTML = NotificationHeaderView.template(headers);
        return this;
    };
    this.renderUser = function(data) {
        document.getElementById('users-list').appendChild(NotificationHeaderView.frdTemplate(data));
    };
    this.initialize();
};
Handlebars.registerPartial("ActionFooter", document.getElementById('action-footer-tpl').innerHTML);
Handlebars.registerPartial("AttachmentAlert", document.getElementById('attachment-alert-tpl').innerHTML);
NotificationHeaderView.frdTemplate = Handlebars.compile(document.getElementById("forward-alert-tpl").innerHTML);
NotificationHeaderView.template = Handlebars.compile(document.getElementById("header-list-tpl").innerHTML);
Handlebars.registerHelper('color', function() {                                                                                          
    if(this.status == "OPEN")
    return "open-status";
    else if(this.status == "CLOSE")
    return "close-status";
    else if(this.status == "INPROGRESS")
    return "inprogress-status";
});



