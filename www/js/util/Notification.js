var createXHR = (function() {
    if (typeof XMLHttpRequest != 'undefined') {
        return function() {
            return new XMLHttpRequest();
        };
    } else if (typeof ActiveXObject != "undefined") {
        return function() {
            if (typeof arguments.callee.activeXString != 'string') {
                var versions = ['MSXML2.XMLHttp.6.0', 'MSXML2.XMLHttp.3.0', 'MSXML2.XMLHttp'], i, len;
                for (i = 0, len = versions.length; i < len; i++) {
                    try {
                        new ActiveXObject(versions[i]);
                        arguments.callee.activeXString = versions[i];
                        break;
                    } catch (ex) {
// Skip if any exception also
                    }
                }
            }
            return new ActiveXObject(arguments.callee.activeXString);
        };
    } else {
        return function() {
            throw new Error('No XHR object available.');
        };
    }
})();


var Notification = function() {
    var self = this;

    this.logIn = function(url, user, password, callback) {

        this.ajax({
            url: url,
            dataType: "json",
            data: "login={\"user_name\": \"" + user + "\" , \"password\": \"" + password + "\"}",
            success: function(data) {
//                alert(JSON.stringify(data));
                callback(data['result']);
            },
            error: function(data) {
                app.showAlert("Ajax Error", JSON.stringify(data));
            }
        });
    };

    this.getHeaders = function(url, callback) {
        this.ajax({
            url: url,
            success: function(data) {
                if (data.response === "SUCCESS") {
                    callback(data);
                } else {
                    app.showAlert(data.response, "Notification Header Request Errored");
                }
            },
            error: function() {
                app.showAlert('Ajax Error');
            }
        });
    };
    this.getDetail = function(url, id, num, callback) {
        this.ajax({
            url: url,
            dataType: "json",
            data: "notify={\"hid\":\"" + id + "\",\"num\":\"" + num +"\"}",
            success: function(data) {
                if (data.response === "SUCCESS") {
                    callback(data);
                }
            },
            error: function() {
                app.showAlert('Ajax Error');
            }
        });
    };

    this.forward = function(url, to_user) {
        this.ajax({
            url: url,
            dataType: "json",
            data: "notify={\"id\": \"" + app.id + "\" , \"user\": \"" + app.user + "\", \"to_user\": \"" + to_user + "\"}",
            success: function(data) {
                app.showAlert("success");
            },
            error: function(data) {
                app.showAlert("Ajax Error");
            }
        });
    };

    this.approve = function(url, msg) {
        this.ajax({
            url: url,
            dataType: "json",
            data: "notify={\"id\": \"" + app.id + "\" , \"user\": \"" + app.user + "\"}",
            success: function(data) {
                app.showAlert("success");
                return "success";
            },
            error: function(data) {
                app.showAlert("Ajax Error");
                return "error"
            }
        });
    };

    this.reject = function(url, msg) {
        this.ajax({
            url: url,
            dataType: "json",
            data: "notify={\"id\": \"" + app.id + "\" , \"user\": \"" + app.user + "\"}",
            success: function(data) {
                app.showAlert("success");
                return "success";
            },
            error: function(data) {
                app.showAlert("Ajax Error");
                return "error";
            }
        });
    };

    this.getUsers = function(url, callback) {
        this.ajax({
            url: url,
            dataType: "json",
            success: function(data) {
                callback(data);
            },
            error: function(data) {
                app.showAlert("Ajax Error");
                return "error";
            }
        });
    };
    this.logOut = function() {
    };

    this.ajax = function(param) {
        var xhr = createXHR();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
//                alert(xhr.status);
                if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
                    param.success(JSON.parse(xhr.responseText));
                } else {
                    param.error();
                }
            }
        };
        if (param.data) {
            xhr.open('GET', param.url + "?" + param.data, true);
        } else {
            xhr.open('GET', param.url, false);
        }
        xhr.send(null);
    };

    return this;

};







try {
// create a static XHR member
    Notification.xhr = createXHR();
} catch (ex) {
    app.showAlert("XHR Obeject not available", "XHR Exception");
}



// Used to simulate async calls. This is done to provide a consistent interface with stores (like WebSqlStore)
// that use async data access APIs
//    var callLater = function(callback, data) {
//        if (callback) {
//            setTimeout(function() {
//                callback(data);
//            });
//        }
//    };
