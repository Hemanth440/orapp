/*notification headerlist headerdetails*/
function toggle(e) {
    var elem = next(e);
    if (elem.style.maxHeight === '1000px') {
        slideUp(elem);
    }
    else {
        slideDown(elem);
    }
}
function slideDown(elem)
{
    if (elem != null) {
        if (elem.className !== 'ui-list-arrow') {
            elem.style.maxHeight = '1000px';
            elem.style.borderBottom = '1px solid #c2c2c2';
        }
    }
}
function slideUp(elem)
{
    elem.style.maxHeight = '0';
    elem.style.borderBottom = '0';
}
function next(elem) {
    do {
        elem = elem.nextSibling;
    } while (elem && elem.nodeType !== 1);
    return elem;
}
document.onclick = function(e) {
    if (document.getElementsByClassName('ui-content')[0] != null) {
        if (e.target.parentNode.parentNode.parentNode) {
            if (e.target.parentNode.parentNode.parentNode.parentNode != null) {
                if (e.target.parentNode.parentNode.parentNode.parentNode.id !== 'ActionHeader' && e.target.parentNode.parentNode.parentNode.id !== 'ActionFooter' && e.target.id !== 'actionIcon') {
                    var elem = document.getElementById('ActionHeader');
                    if (elem !== null)
                        slideUp(elem);
                }
            }
        }
    }
};
/*notification headerlist headerdetails*/

/*header details*/
function toggleAll(e) {
    if (e.innerHTML === 'Expand')
        e.innerHTML = 'Shrink';
    else
        e.innerHTML = 'Expand';
    var container = document.getElementById("listContainer");
    var listTag = document.getElementsByTagName('li');
    for (var i = 0; i < container.childNodes.length; i++) {
        if (container.childNodes[i].nodeName === 'LI') {
            var elem = container.childNodes[i].getElementsByClassName('ui-list-details')[0];
            if (e.innerHTML === 'Shrink')
                slideDown(elem);
            else
                slideUp(elem);
        }
    }
}
/*contact us*/
function clearForm(id) {
    var elem = document.getElementById(id);
    elem.reset();
}
/*contact us*/

/*header details*/
function closeActionHeader() {
    var elem = document.getElementById('ActionHeader');
    slideUp(elem);
}
/*header details*/

/*notification headerlist headerdetails start*/
window.onorientationchange = function() {
    var hash = location.pathname.split('/');
    var target = hash[hash.length - 1];
    var target = target.split('.')[0];
    if (target === 'index' || target === '')
        DocumentCenter('loginScreen');
    if (document.getElementsByClassName('ui-content')[0].offsetHeight > window.innerHeight)
        setContentHeight();
    else
        setContentHeightLess();
};

function DocumentCenter(id) {
    var windowHeight = window.innerHeight;
    var elem = document.getElementById(id);
    if (elem !== null) {
        var documentHeight = elem.offsetHeight;
        var topSpace;
        if (windowHeight > documentHeight)
            topSpace = (windowHeight - documentHeight) / 2;
        else
            topSpace = (documentHeight - windowHeight) / 2;
        if ((topSpace - 20) > 0)
            elem.style.top = (topSpace - 20) + "px";
        else {
            elem.style.top = '';
        }
        if (elem.parentNode.offsetWidth > 0 || elem.parentNode.offsetHeight > 0) {
            if (elem.parentNode.offsetHeight > elem.offsetHeight) {
                elem.parentNode.style.height = elem.offsetHeight + 2 * topSpace + "px";
                document.getElementById('screen').style.height = elem.offsetHeight + 2 * topSpace + "px";
                document.getElementById('screen').style.overflow = 'hidden';
                if (topSpace > 0) {
                    elem.style.top = topSpace + "px";
                }
                else
                    elem.style.top = '';
            }
        }
    }
}
/*notification headerlist headerdetails end*/

function alertCenter(id) {
    var windowHeight = window.innerHeight;
    if (document.getElementById('screen') !== null) {
        document.getElementById('screen').style.height = windowHeight + 'px';
        document.getElementById('screen').style.overflow = 'hidden';
        document.getElementById(id).parentNode.style.height = windowHeight + 'px';
    }
    DocumentCenter(id);
}
/*headerdetails start*/
function showAlert(id) {
    var elem = document.getElementById(id);
    elem.parentNode.className = elem.parentNode.className + ' ui-popup-show';
    elem.className = elem.className + ' ui-popup-show';
    DocumentCenter(id);
}
function closeAlert(elem) {
    var commentClear1 = document.getElementById('comment1');
    var commentClear2 = document.getElementById('comment2');
    commentClear1.value = "";
    commentClear2.value = "";
    elem.parentNode.parentNode.className = '';
    elem.parentNode.className = '';
    elem.parentNode.parentNode.className = 'ui-pop-up-wrapper';
    elem.parentNode.className = 'ui-pop-up-content';
    document.getElementById('screen').removeAttribute('style');
    document.getElementById('screen').style.height = 'auto';
    elem.parentNode.parentNode.removeAttribute('style');
}

/*headerdetails end*/

/*All pages*/
window.onload = function() {
    setContentHeight();
    DocumentCenter('loginScreen');
};

function setContentHeight() {
    if (document.getElementsByClassName('ui-content')[0] != null) {
        if (document.getElementsByClassName('ui-content')[0].offsetHeight > window.innerHeight) {
            document.getElementsByClassName('ui-content')[0].style.height = window.innerHeight - (document.getElementsByClassName('ui-footer')[0].offsetHeight +
                    document.getElementsByClassName('ui-header')[0].offsetHeight) + "px"
        }
        else if (document.getElementsByClassName('ui-content')[0].offsetHeight < window.innerHeight) {
            document.getElementsByClassName('ui-content')[0].style.height = window.innerHeight - (document.getElementsByClassName('ui-footer')[0].offsetHeight +
                    document.getElementsByClassName('ui-header')[0].offsetHeight) + "px"
            document.getElementById('scroller').style.top = '0';
        }
    }
}
function tab(elem) {
    var index = $(elem).index();
    index = index + 1;
    $("#ui-tab-content>div").removeClass('show');
    $("#ui-tab-content>div:nth-child(" + index + ")").addClass('show');
}
/*All pages*/