var app = {
    showAlert: function(message, title) {
        console.log('alerted: ' + message + ' : ' + title);
        if (navigator.notification) {
            navigator.notification.alert(message, null, title, 'OK');
        } else {
            alert(title ? (title + ": " + message) : message);
        }
    },
    findMatch: function(ptrn) {
        switch (ptrn) {
            case "rqstns":
                return "Requisition";
            case "xpnsap":
                return "Expense Approval";
            case "prchrd":
                return "Purchase Requisition";
            case "pblnvc":
                return "Payable Invoice";
            default:
                this.showAlert("No type match", "Find Match");
                return "";
        }
    },
    route: function() {
        /*
         * If there is no hash tag in the URL: display the Home View 
         * if there is a hash tag matching pattern for any URL call the URL action defined in the notify object
         */
        var hash = window.location.hash, self = this;
        if (!hash) {
            if (this.prevPage) {
                this.slidePage(this.prevPage);
            } else if (this.homePage) {
                this.slidePage(this.homePage);
            } else {
                this.homePage = new LoginView().render();
                this.slidePage(this.homePage);
            }
            return;
        }

        var match;
        var match = hash.match(/^#Home/);
        if (match) {
            this.slidePage(this.homePage);
        }

        match = hash.match(app.loginURL);
        if (match) {
            this.notify.logIn(self.URL + "Login", $('#user-name').val(), $('#password').val(), function(data) {
                data.head = "Notifications";
                data.back = '';
                self.homePage = new NotificationsView(data).render();
                self.currentPage = null;
                self.slidePage(self.homePage);
                document.getElementById('notification-home-back').style.display = 'none';
                document.getElementById('actionIcon').style.display = 'none';
            });
            return;
        }

        match = hash.match(app.notifyURL);
        if (match) {
            this.notify.getHeaders(self.URL + "Notifications/" + match[1] + "/1", function(data) {
                data.head = self.findMatch(match[1]);
                data.back = 'Home';
                self.prevPage = new NotificationHeaderView(data).render();
                self.slidePage(self.prevPage);
                document.getElementById('actionIcon').style.display = 'none';
            });
            return;
        }

        match = hash.match(app.detailsURL);
        if (match) {
//            app.id = Number(match[2]);
            this.notify.getDetail(self.URL + "Detail/" + match[1], match[2], match[3], function(data) {
                data.head = "Details";
                data.back = '';
                self.slidePage(new NotificationDetailView(data).render());
            });
            return;
        }

        match = hash.match(app.forwardURL);
        if (match) {
            this.notify.forward(self.URL + "Forward");
            return;
        }

        match = hash.match(app.approveURL);
        if (match) {
            this.notify.approve(self.URL + "Approve");
            return;
        }

        match = hash.match(app.rejectURL);
        if (match) {
            this.notify.reject(self.URL + "Reject");
            return;
        }

    },
    registerEvents: function() {
        var self = this;
        //check of browser if support touch events
        if (document.documentElement.hasOwnProperty('ontouchstart')) {
            //   if yes: Register the touch event listener to change the selected state of the item
            $('body').on('touchstart', 'a', function(event) {
                $(event.target).addClass('tappable-active');
            });
            $('body').on('touchend', 'a', function(event) {
                $(event.target).removeClass('tappable-active');
            });
        } else {
            //   if not: register mouce events instead
            $('body').on('moucedown', 'a', function(event) {
                $(event.target).addClass('tappable-active');
            });
            $('body').on('mouceup', 'a', function(event) {
                $(event.target).removeClass('tappable-active');
            });
        }

        // Add an Event Listener to Listen to URL hash tag Changes
        $(window).on('hashchange', $.proxy(this.route, this));
    },
    initialize: function() {
//        var self = this;
        this.registerEvents();
        this.URL = "http://192.168.10.50:8084/ebs/";
        this.notifyURL = /^#Not\/(.{6})/;
        this.loginURL = /^#Login/;
        this.forwardURL = /^#Forward/;
        this.approveURL = /^#Approve/;
        this.rejectURL = /^#Reject/;
        this.detailsURL = /^#Detail(.{6})\/(\d{1,})-(.{1,})/;
        this.notify = new Notification();
        this.route();
    },
    slidePage: function(page) {        
        var currentPageDest,
                self = this;

        // If there is no current page (app just started) -> No transition: Position new page in the view port
        if (!this.currentPage) {
            $(page.el).attr('class', 'page');
            $('body').html(page.el);
            this.currentPage = page;
            return;
        }

        // Cleaning up: remove old pages that were moved out of the viewport
        $('.stage-right, .stage-left').not('.homePage').remove();

        if (page === app.homePage) {
            // Always apply a Back transition (slide from left) when we go back to the search page
            $(page.el).attr('class', 'page stage-left');
            currentPageDest = "stage-right";
        } else {
            // Forward transition (slide from right)
            $(page.el).attr('class', 'page stage-right');
            currentPageDest = "stage-left";
        }

        $('body').append(page.el);

        // Wait until the new page has been added to the DOM...
        setTimeout(function() {
            // Slide out the current page: If new page slides from the right -> slide current page to the left, and vice versa
            $(self.currentPage.el).attr('class', 'page transition ' + currentPageDest);
            // Slide in the new page
            $(page.el).attr('class', 'page stage-center transition');
            self.currentPage = page;
        });

    }

};

app.initialize();