

var NotificationDetailView = function(detail){
    this.initialize = function() {
        // Define a div wrapper for the view. The div wrapper is used to attach events.
        this.el = document.createElement('div');
    };
    this.render = function(action) {
        this.el.innerHTML = NotificationDetailView.template(detail);
        return this;
    };
    
    this.initialize();
};

NotificationDetailView.template = Handlebars.compile(document.getElementById("header-details-tpl").innerHTML);

